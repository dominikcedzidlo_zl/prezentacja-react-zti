import React, { Component } from 'react';
import TimeRanges from './components/TimeRanges';
import TimeRangesInText from './components/TimeRangesInText';
import './App.css';

function timeRangeTemplate() {
  return {
    id: Math.random(),
    text: '',
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    let timeRanges = [
      {
        id: 0,
        text: '12:00-13:30',
      }
    ]
    let history = [timeRanges]
    this.state = {
      history,
      historyIndex: 0,
    }
  }

  currentTimeRanges = () => {
    const {
      history,
      historyIndex,
    } = this.state
    return history[historyIndex]
  }

  setTimeRanges = (timeRanges) => {
    const {
      history,
      historyIndex,
    } = this.state
    this.setState({
      history: [...history.slice(0, historyIndex+1), timeRanges],
      historyIndex: historyIndex+1,
    })
  }

  updateTimeRange = tr => {
    const timeRanges = this.currentTimeRanges()
    this.setTimeRanges(timeRanges.map(otr => (otr.id === tr.id) ? tr : otr))
  }

  removeTimeRange = tr => {
    const timeRanges = this.currentTimeRanges()
    this.setTimeRanges(timeRanges.filter(otr => otr.id !== tr.id))
  }

  appendNewTimeRange = () => {
    const timeRanges = this.currentTimeRanges()
    this.setTimeRanges([...timeRanges, timeRangeTemplate()])
  }

  undoAble = () => {
    const {
      historyIndex,
    } = this.state
    return historyIndex > 0
  }

  redoAble = () => {
    const {
      history,
      historyIndex,
    } = this.state
    return historyIndex < history.length-1
  }

  handleUndo = () => {
    const {
      historyIndex,
    } = this.state
    if (historyIndex > 0) {
      this.setState({
        historyIndex: historyIndex-1,
      })
    }
  }

  handleRedo = () => {
    const {
      history,
      historyIndex,
    } = this.state
    if (historyIndex < history.length-1) {
      this.setState({
        historyIndex: historyIndex+1,
      })
    }
  }

  render() {
    const timeRanges = this.currentTimeRanges()
    return (
      <div>
        <div className="container">
          <h1>Kalkulator czasu</h1>
          <button onClick={this.handleUndo} disabled={!this.undoAble()}>cofnij</button>
          <button onClick={this.handleRedo} disabled={!this.redoAble()}>przywróć</button>
          <br/>
          <br/>
          <TimeRanges
            timeRanges={timeRanges}
            updateTimeRange={this.updateTimeRange}
            appendNewTimeRange={this.appendNewTimeRange}
            removeTimeRange={this.removeTimeRange}
          />
          <br/>
          <br/>
          <TimeRangesInText
            timeRanges={timeRanges}
          />
        </div>
      </div>
    );
  }
}

export default App;
