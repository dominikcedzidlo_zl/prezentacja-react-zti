import React from 'react';
import TimeRange from './TimeRange';
import { timeRangeToMinutes, minutesToStr } from '../utils'

function TimeRanges({ timeRanges, updateTimeRange, appendNewTimeRange, removeTimeRange }) {
  return (
    <div>
      <button onClick={appendNewTimeRange}>dodaj okres</button>
      <br/>
      <br/>
      {timeRanges.length > 0 ?
        (
          <div>
            {timeRanges.map(tr => (
              <TimeRange
                key={tr.id}
                timeRange={tr}
                onTimeRangeChange={updateTimeRange}
                onTimeRangeRemove={removeTimeRange}
              />)
            )}
            <div>Suma: {minutesToStr(
              timeRanges.map(tr => timeRangeToMinutes(tr, 0)).reduce((s, b) => s+b)
            )}</div>

          </div>
        ) : (
          <div>
            Brak Okresów
          </div>
        )
      }
    </div>
  )
}

export default TimeRanges
