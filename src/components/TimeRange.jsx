import React, { Component } from 'react';
import { timeRangeToStr } from '../utils'

class TimeRange extends Component {
  handleChange = e => {
    let {
      timeRange,
      onTimeRangeChange,
     } = this.props;

    onTimeRangeChange({
      ...timeRange,
      text: e.target.value,
    })
  }

  handleRemove = e => {
    let {
      timeRange,
      onTimeRangeRemove,
    } = this.props;

    onTimeRangeRemove(timeRange)
  }

  render() {
    let {
      timeRange,
    } = this.props;

    return (
      <div>
        <button onClick={this.handleRemove}> usuń </button>
        <input value={timeRange.text} onChange={this.handleChange}/>
        {" = "}
        {timeRangeToStr(timeRange)}
      </div>
    )
  }
}

export default TimeRange
