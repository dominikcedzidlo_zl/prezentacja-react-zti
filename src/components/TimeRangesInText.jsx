import React from 'react';
import { timeRangeToStr } from '../utils'


function TimeRangesInText({timeRanges}) {
  const text = timeRanges.map(timeRange => timeRange.text + '=' + timeRangeToStr(timeRange)).join('\n')
  return (
    <div>
      <textarea rows={timeRanges.length} value={text} readOnly></textarea>
    </div>
  )
}

export default TimeRangesInText
