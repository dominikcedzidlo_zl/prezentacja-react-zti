
const textRe = /^(\d+):(\d+)-(\d+):(\d+)$/

export function timeRangeToMinutes(tr, defaultMinutes) {
  let { text } = tr
  let m = textRe.exec(text)

  if(m) {
    let start = Number(m[1])*60 + Number(m[2])
    let stop = Number(m[3]*60 + Number(m[4]))
    let minutes = stop-start
    return minutes
  } else {
    return defaultMinutes
  }
}

export function minutesToStr(minutes) {
  let hours = Math.floor(minutes/60)
  minutes = minutes - hours*60
  return `${hours}h${minutes}m`
}

export function timeRangeToStr(tr) {
  const minutes = timeRangeToMinutes(tr)
  if (minutes === undefined) {
    return '?'
  } else {
    return minutesToStr(minutes)
  }
}
